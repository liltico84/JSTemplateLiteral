/*
Template literal
*/

var DogPhotos = [
    {
        id: "Foxy",
        title: "Fox Terriers are two different breeds of the terrier dog type: the Smooth Fox Terrier and the Wire Fox Terrier. ",
        imageUrl: "https://res.cloudinary.com/liltico/image/upload/v1653214688/JSLearning/fox_terrier_ljazb4.jpg",
    },
    {
        id: "Husky",
        title: "Husky is a general term for a dog used in the polar regions, primarily and specifically for work as sled dogs.",
        imageUrl: "https://res.cloudinary.com/liltico/image/upload/v1653214688/JSLearning/husky_rg3m2p.jpg",
    },
    {
        id: "Bulldog",
        title: "The Bulldog is a British breed of dog of mastiff type.",
        imageUrl: "https://res.cloudinary.com/liltico/image/upload/v1653214688/JSLearning/bulldog_pel2gp.jpg",
    },
    {
        id: "Terrier",
        title: "Terrier is a type of dog originally bred to hunt vermin.",
        imageUrl: "https://res.cloudinary.com/liltico/image/upload/v1653214688/JSLearning/terrier_fretrx.jpg",
    }
];

var photoListTemplate = '';

for (var DogPhoto of DogPhotos) {
    photoListTemplate = photoListTemplate + `
    <div>
        #${DogPhoto.id} <br>
        ${DogPhoto.title}
        <img src="${DogPhoto.imageUrl}" alt="dog photo">
    </div>`;
}

document.getElementById("photo-list-container").innerHTML = photoListTemplate;